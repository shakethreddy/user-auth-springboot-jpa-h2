package com.jeppiaar.userauth.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.jeppiaar.userauth.model.User;

public interface UserAuthRepository extends JpaRepository<User, Long>{

	   User findByusername(String username);
}
