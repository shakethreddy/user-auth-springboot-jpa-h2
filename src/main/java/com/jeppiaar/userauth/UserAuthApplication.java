package com.jeppiaar.userauth;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.jeppiaar.userauth.model.User;
import com.jeppiaar.userauth.repository.UserAuthRepository;

@SpringBootApplication
public class UserAuthApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserAuthApplication.class, args);
		System.out.println("test");
	}
	 @Bean
     CommandLineRunner init (UserAuthRepository userRepo){
         return args -> {
             userRepo.save(new User(1,"shaketh","reddy","admin@jeppiaar.com","Admin", "Active", "reddyshaketh", "reddy@123"));
         };
     }

}
