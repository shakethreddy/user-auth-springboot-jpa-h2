package com.jeppiaar.userauth.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jeppiaar.userauth.model.User;
import com.jeppiaar.userauth.repository.UserAuthRepository;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping("/api")
public class UserAuthController {

	@Autowired
	UserAuthRepository usrAuthRepo;
	
	/*
	 * @Autowired UserAuthService userAuthSvc;
	 */
	
	@GetMapping("/users")
	public ResponseEntity<List<User>> getAllUser() {
		try {
			List<User> users = new ArrayList<>();

			usrAuthRepo.findAll().forEach(users::add);

			if (users.isEmpty()) {
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<>(users, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@GetMapping("/users/{id}")
	public ResponseEntity<User> getUserById(@PathVariable("id") long id) {
		Optional<User> userData = usrAuthRepo.findById(id);

		if (userData.isPresent()) {
			return new ResponseEntity<>(userData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping("/users/register")
	public ResponseEntity<User> createUser(@RequestBody User user) {
		try {
			user.setStatus("Active");
			User _user = usrAuthRepo
					.save(user);
			return new ResponseEntity<>(_user, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("/users/{id}")
	public ResponseEntity<User> updateUser(@PathVariable("id") long id, @RequestBody User usr) {
		Optional<User> userData = usrAuthRepo.findById(id);

		if (userData.isPresent()) {
			User _user = userData.get();
			_user.setFirstName(usr.getFirstName());
			_user.setLastName(usr.getLastName());
			_user.setEmail(usr.getEmail());
			_user.setRole(usr.getRole());
			_user.setStatus(usr.getStatus());
			return new ResponseEntity<>(usrAuthRepo.save(_user), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/users/{id}")
	public ResponseEntity<HttpStatus> deleteUser(@PathVariable("id") long id) {
		try {
			usrAuthRepo.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/users")
	public ResponseEntity<HttpStatus> deleteAllUsers() {
		try {
			usrAuthRepo.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
	
    @PostMapping("/users/authenticate")
    public User login(@RequestBody User user){
		User usr=usrAuthRepo.findByusername(user.getUsername());
		if((user.getUsername().equals(usr.getUsername())) && (user.getPassword().equals(usr.getPassword()))) {
			return usr;
		}
		return usr;
    }
	
}
