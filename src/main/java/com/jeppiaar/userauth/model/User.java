package com.jeppiaar.userauth.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user")
public class User {

		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private long id;

		@Column(name = "FIRST_NAME")
		private String firstName;
		
		@Column(name = "LAST_NAME")
		private String lastName;

		@Column(name = "EMAIl")
		private String email;

		@Column(name = "ROLE")
		private String role;
		
		@Column(name = "STATUS")
		private String status;
		
		@Column(name = "USER_NAME")
		private String username;
		
		@Column(name = "PASSWORD")
		private String password;
		
		public User() {
			
		}
		public User(long id, String firstName, String lastName, String email, String role, String status,
				String username, String password) {
			super();
			this.id = id;
			this.firstName = firstName;
			this.lastName = lastName;
			this.email = email;
			this.role = role;
			this.status = status;
			this.username = username;
			this.password = password;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public String getRole() {
			return role;
		}

		public void setRole(String role) {
			this.role = role;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public long getId() {
			return id;
		}
		
		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		@Override
		public String toString() {
			return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
					+ ", role=" + role + ", status=" + status + ", username=" + username + ", password=" + password
					+ "]";
		}
		
}
